﻿/**
 * Projet: ADN
 * Auteur: Zackary Ioset
 * Détails: Calcul du nombre d'occurence des bases ACTG
 * Date: 14 octobre 2022
 * Version:1.0
*/


using System;
using System.IO;


namespace ADN
{
    internal class Program
    {
        static void Main(string[] args)
        {
            StreamReader s;
            s = new StreamReader("C:\\Users\\Zackary.ist\\Desktop\\2022-2023\\chromosome-11-partial\\chromosome-11-partial.txt");
            String line;
            line = s.ReadLine();

            int compteurA = 0;
            int compteurT = 0;
            int compteurG = 0;
            int compteurC = 0;


            while (line != null)
            {

                for (int i = 0; i < line.Length; i = i + 1)
                {

                    if (line[i] == 'A')
                    {
                        compteurA = compteurA + 1;
                    }

                    if (line[i] == 'T')
                    {
                        compteurT = compteurT + 1;
                    }

                    if (line[i] == 'G')
                    {
                        compteurG = compteurG + 1;
                    }

                    if (line[i] == 'C')
                    {
                        compteurC = compteurC + 1;
                    }
                }
                line = s.ReadLine();
            }
            Console.WriteLine("Nombres de A = " + compteurA);
            Console.WriteLine("Nombres de T = " + compteurT);
            Console.WriteLine("Nombres de G = " + compteurG);
            Console.WriteLine("Nombres de C = " + compteurC);
            Console.ReadKey();
        }

    }
}

